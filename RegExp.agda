{-# OPTIONS --cubical #-}
module RegExp where


open import Cubical.Foundations.Prelude
  using (_≡_
        ; refl
        ; cong
        ; sym
        ; _∙_
        ; subst
        ; step-≡
        ; _∎
        ; funExt
        ; Level
        ; ℓ-max)

open import Cubical.Relation.Nullary
  using (Dec ; no ; yes ;  ¬_ )

open import Cubical.Data.Empty
  renaming (rec to ⊥!)

variable
  ℓ : Level

open import Data.List as List
  using (List ; _∷_ ; [] ; _++_ ; length)

open import Data.Nat
  using (ℕ ; suc ; zero) -- ;  _+_ ; _≤_ ; z≤n ; s≤s ; _⊔_ ; _<_ )
open import Cubical.Data.Nat
  using (snotz)

open import Data.Product
  using (_×_ ; ∃ ; Σ-syntax ; _,_)

open import Data.Sum
  using (_⊎_ ; inj₁ ; inj₂)

open import Data.Bool
  using (Bool ; true ; false ; _∧_ ; _∨_ ; not)

{-data RegExp (A : Set ℓ) : Set ℓ where
  EmptySet : RegExp A
  EmptyStr : RegExp A
  Char : A → RegExp A
  App : RegExp A → RegExp A → RegExp A
  Union : RegExp A → RegExp A → RegExp A
  Intersection : RegExp A → RegExp A → RegExp A
  Not : RegExp A → RegExp A
  Star : RegExp A → RegExp A

data _=~_ {A : Set ℓ} {d : DecEq A} : List A → RegExp A → Set ℓ where
  MNot : (l : List A) (re : RegExp A) → ¬ (l =~ re) → l =~ Not re
-}

data RegExp (A : Set ℓ) : Set ℓ where
  EmptySet : RegExp A
  EmptyStr : RegExp A
  Char : A → RegExp A
  App : RegExp A → RegExp A → RegExp A
  Union : RegExp A → RegExp A → RegExp A
  Star : RegExp A → RegExp A
  

data ExpMatch {A : Set ℓ} : List A → RegExp A → Set ℓ where
  MEmptyStr : ExpMatch [] EmptyStr
  MChar : (x : A) → ExpMatch (x ∷ []) (Char x)
  MApp : (l₁ l₂ : List A) (re₁ re₂ : RegExp A)
       → ExpMatch l₁ re₁ → ExpMatch l₂ re₂ 
       → ExpMatch (l₁ ++ l₂) (App re₁ re₂)
  MUnionL : (l : List A) (re₁ re₂ : RegExp A)
    → ExpMatch l re₁ → ExpMatch l (Union re₁ re₂)
  MUnionR : (l : List A) (re₁ re₂ : RegExp A)
    → ExpMatch l re₂ → ExpMatch l (Union re₁ re₂)
{-    
  MInter : (l : List A) (r s : RegExp A)
    → ExpMatch l r → ExpMatch l s → ExpMatch l (Intersection r s)
-}
  MStarZ : (re : RegExp A) → ExpMatch [] (Star re)
  MStarS : (l₁ l₂ : List A) (re : RegExp A)
    → ExpMatch l₁ re → ExpMatch l₂ (Star re)
    → ExpMatch (l₁ ++ l₂) (Star re)

_=~_ : {A : Set ℓ} (l : List A) (re : RegExp A) → Set ℓ
l =~ re = ExpMatch l re

id-ℕ : ℕ → ℕ
id-ℕ x = x

_+_ : ℕ → ℕ → ℕ
zero + n = n
suc m + n = suc (m + n)

infixl 6 _+_

+-suc : (m n : ℕ) → m + suc n ≡ suc (m + n)
+-suc zero n = refl
+-suc (suc m) n = cong suc (+-suc m n)

+-comm : (m n : ℕ) → (m + n) ≡ (n + m)
+-comm zero zero = refl
+-comm zero (suc n) = cong suc (+-comm zero n)
+-comm (suc m) n = suc (m + n) ≡⟨ cong suc (+-comm m n) ⟩
                   suc (n + m) ≡⟨ sym (+-suc n m) ⟩ refl

+-assoc : (m n o : ℕ) → m + (n + o) ≡ m + n + o
+-assoc zero n o = refl
+-assoc (suc m) n o = cong suc (+-assoc m n o)

_⊔_ : ℕ → ℕ → ℕ
zero ⊔ n = n
suc m ⊔ zero = suc m
suc m ⊔ suc n = suc (m ⊔ n)

data _≤_ : ℕ → ℕ → Set where
  z≤n : {n : ℕ} → zero ≤ n
  s≤s : {m n : ℕ} → m ≤ n → suc m ≤ suc n

infix 4 _≤_

_<_ : (m n : ℕ) → Set
m < n = suc m ≤ n

≤-split : (m n : ℕ) → m ≤ n ⊎ (n < m)
≤-split zero n = inj₁ z≤n
≤-split (suc m) zero = inj₂ (s≤s z≤n)
≤-split (suc m) (suc n) with ≤-split m n
≤-split (suc m) (suc n) | inj₁ q = inj₁ (s≤s q)
≤-split (suc m) (suc n) | inj₂ q = inj₂ (s≤s q)

suc' : ℕ → ℕ
suc' x = x + 1

{- Definitional -}
test₁ : (n : ℕ) → (suc n ≡ 1 + n)
test₁ n = refl

{- Not definitional -}
test₂ : (n : ℕ) → (suc n ≡ suc' n)
test₂ zero = refl
test₂ (suc n) = cong suc (test₂ n)

test₃ : (id-ℕ ∷ []) =~ Char id-ℕ
test₃ = MChar id-ℕ

test₄ : (suc ∷ []) =~ Char suc 
test₄ = MChar suc 

test₅ : (suc' ∷ []) =~ Char suc'
test₅ = MChar suc'

test₆ : (((λ x → x)) ∷ suc' ∷ []) =~ App (Char id-ℕ) (Char suc)
test₆ = subst B p (MApp (id-ℕ ∷ []) (suc ∷ []) (Char id-ℕ) (Char suc) test₃ test₄)
  where
    B : (ℕ → ℕ) → Set₀
    B f = (((λ x → x)) ∷ f ∷ []) =~ App (Char id-ℕ) (Char suc)
    p : suc ≡ suc'
    p = funExt test₂

_≢_ : {A : Set ℓ} → A → A → Set ℓ
x ≢ y = ¬ (x ≡ y)

pumpingConstant : {A : Set ℓ} → RegExp A → ℕ
pumpingConstant EmptySet = 1
pumpingConstant EmptyStr = 1
pumpingConstant (Char _) = 2
pumpingConstant (App re₁ re₂) = pumpingConstant re₁ + pumpingConstant re₂
pumpingConstant (Union re₁ re₂) = pumpingConstant re₁ ⊔ pumpingConstant re₂
pumpingConstant (Star re) = pumpingConstant re

napp : {A : Set ℓ} → ℕ → List A → List A
napp zero l = []
napp (suc n) l = l ++ napp n l

length-++ : {A : Set ℓ} (l₁ l₂ : List A) → length (l₁ ++ l₂) ≡ (length l₁ + length l₂) 
length-++ [] l₂ = refl
length-++ (x ∷ l₁) l₂ = cong suc (length-++ l₁ l₂)

++-assoc : {A : Set ℓ} (l₁ l₂ l₃ : List A) → (l₁ ++ l₂) ++ l₃ ≡ l₁ ++ l₂ ++ l₃
++-assoc [] l₂ l₃ = refl
++-assoc (x ∷ l₁) l₂ l₃ = cong (λ s → x ∷ s ) (++-assoc l₁ l₂ l₃)

≤-z-is-z : {n : ℕ} → n ≤ 0 → n ≡ 0
≤-z-is-z {zero} p = refl

≤-s : {n : ℕ} → n ≤ suc n
≤-s {zero} = z≤n
≤-s {suc n} = s≤s ≤-s

≤-refl : {m : ℕ} → m ≤ m
≤-refl {zero} = z≤n
≤-refl {suc m} = s≤s ≤-refl

≤-tran : {m n o : ℕ} → m ≤ n → n ≤ o → m ≤ o
≤-tran z≤n q = z≤n
≤-tran (s≤s p) (s≤s q) = s≤s (≤-tran p q)

≤-+-left : {m n o : ℕ} → m ≤ n → m ≤ o + n
≤-+-left {m} {n} {zero} p = p
≤-+-left {m} {n} {suc o} p = ≤-tran (≤-+-left {m} {n} {o} p) ≤-s

≤-+-right : {m n o : ℕ} → m ≤ n → m ≤ n + o
≤-+-right {m} {n} {o} p = subst (m ≤_) (+-comm o n) (≤-+-left p)

≤-⊔-left : {m n : ℕ} → m ≤ m ⊔ n
≤-⊔-left {zero} {n} = z≤n
≤-⊔-left {suc m} {zero} = ≤-refl
≤-⊔-left {suc m} {suc n} = s≤s ≤-⊔-left 

≤-⊔-right : {m n : ℕ} → n ≤ m ⊔ n
≤-⊔-right {zero} {n} = ≤-refl
≤-⊔-right {suc m} {zero} = z≤n
≤-⊔-right {suc m} {suc n} = s≤s ≤-⊔-right

bound-args-⊔ : {m n o : ℕ} → m ⊔ n ≤ o → m ≤ o × n ≤ o
bound-args-⊔ {zero} {n} {o} p = z≤n , p
bound-args-⊔ {suc m} {zero} {o} p = p , z≤n
bound-args-⊔ {suc m} {suc n} {suc o} (s≤s p) with bound-args-⊔ {m} {n} {o} p
... | q₁ , q₂ = s≤s q₁ , s≤s q₂


flat-ℕ : ℕ → ℕ 
flat-ℕ zero = 0
flat-ℕ (suc n) = 1

flat-ℕ-max : {m n : ℕ} → flat-ℕ m ≡ 1 → flat-ℕ n ≡ 1 → flat-ℕ (m ⊔ n) ≡ 1
flat-ℕ-max {zero} {n} p q = q
flat-ℕ-max {suc m} {zero} p q = p 
flat-ℕ-max {suc m} {suc n} p q = refl

flat-ℕ-+ : {m n : ℕ} → flat-ℕ m ≡ 1 → flat-ℕ n ≡ 1 → flat-ℕ (m + n) ≡ 1
flat-ℕ-+ {zero} {n} p q = q
flat-ℕ-+ {suc m} {n} p q = p

total-ℕ : (m n : ℕ) → m ≤ n ⊎ n ≤ suc m
total-ℕ zero n = inj₁ z≤n
total-ℕ (suc m) zero = inj₂ z≤n
total-ℕ (suc m) (suc n) with total-ℕ m n
... | inj₁ p = inj₁ (s≤s p)
... | inj₂ q = inj₂ (s≤s q)

summands-are-≤ : {a b c : ℕ} → a + b ≤ c → a ≤ c × b ≤ c
summands-are-≤ {zero} {b} {c} p = z≤n , p
summands-are-≤ {suc a} {b} {.(suc _)} (s≤s p) with summands-are-≤ {a} {b} {_} p
... | q₁ , q₂ = s≤s q₁ , ι q₂
  where
    ι : {m n : ℕ} → m ≤ n → m ≤ suc n
    ι z≤n = z≤n
    ι (s≤s p) = s≤s (ι p)

break-≤-sum : {a b c d : ℕ}  → a + b ≤ c + d → a ≤ c ⊎ ((c < a) × (b ≤ d))
break-≤-sum {zero} {b} {c} {d} p = inj₁ z≤n
break-≤-sum {suc a} {b} {zero} {d} p with summands-are-≤ {suc a} {b} {d} p
break-≤-sum {suc a} {b} {zero} {d} p | q , r = inj₂ (s≤s z≤n , r)
break-≤-sum {suc a} {b} {suc c} {d} (s≤s p) with break-≤-sum {a} {b} {c} {d} p
break-≤-sum {suc a} {b} {suc c} {d} (s≤s p) | inj₁ q₁ = inj₁ (s≤s q₁)
break-≤-sum {suc a} {b} {suc c} {d} (s≤s p) | inj₂ (r , q₂) = inj₂ (s≤s r , q₂)

+-≤-both : {a b c d : ℕ} → a ≤ c → b ≤ d → (a + b) ≤ (c + d)
+-≤-both {.zero} {b} {c} {d} z≤n q = ≤-+-left q
+-≤-both {.(suc _)} {b} {.(suc _)} {d} (s≤s p) q = s≤s (+-≤-both p q)

pcLemma : {A : Set ℓ} (re : RegExp A) → flat-ℕ (pumpingConstant re) ≡ 1
pcLemma EmptySet = refl
pcLemma EmptyStr = refl
pcLemma (Char x) = refl
pcLemma (App re₁ re₂) = flat-ℕ-+ (pcLemma re₁) (pcLemma re₂)
pcLemma (Union re₁ re₂) = flat-ℕ-max (pcLemma re₁) (pcLemma re₂)
pcLemma (Star re) = flat-ℕ (pumpingConstant re) ≡⟨ pcLemma re ⟩ 1 ∎

pumpingLemma : {A : Set ℓ} (s : List A) (re : RegExp A)
  → s =~ re → pumpingConstant re ≤ length s
  → Σ[ s₁ ∈ List A ] Σ[ s₂ ∈ List A ] Σ[ s₃ ∈ List A ]
    (s ≡ s₁ ++ s₂ ++ s₃ × (s₂ ≢ [])
    × length s₁ + length s₂ ≤ pumpingConstant re
    × ((n : ℕ) → (s₁ ++ napp n s₂ ++ s₃) =~ re))
-- pumpingConstant re ≤ length s is an absurdity Agda will recognize with enough case splitting
-- on p though it does warn that case-splitting will mess up computations involing stransport
-- lookup injectivity of constructors in a cubical setting
pumpingLemma .(x ∷ []) .(Char x) (MChar x) (s≤s ())
pumpingLemma .(l₁ ++ l₂) .(App re₁ re₂) (MApp l₁ l₂ re₁ re₂ m₁ m₂) p with break-≤-sum {c₁} {c₂} {length l₁} {length l₂} a
  where
    c₁ : ℕ
    c₁ = pumpingConstant re₁
    c₂ : ℕ
    c₂ = pumpingConstant re₂
    a : (pumpingConstant re₁ + pumpingConstant re₂) ≤ (length l₁ + length l₂)
    a = subst (λ x → (c₁ + c₂) ≤ x) (length-++ l₁ l₂) p
pumpingLemma .(l₁ ++ l₂) .(App re₁ re₂) (MApp l₁ l₂ re₁ re₂ m₁ m₂) p | inj₁ q₁ with pumpingLemma l₁ re₁ m₁ q₁
pumpingLemma .(l₁ ++ l₂) .(App re₁ re₂) (MApp l₁ l₂ re₁ re₂ m₁ m₂) p | inj₁ q₁ | s₁ , s₂ , s₃ , η , u , ι , f
  = s₁ , s₂ , s₃ ++ l₂ , η' , u , ι' , g
    where
      η' : l₁ ++ l₂ ≡ s₁ ++ s₂ ++ s₃ ++ l₂
      η' = l₁ ++ l₂ ≡⟨ cong (_++ l₂) η ⟩
           (s₁ ++ s₂ ++ s₃) ++ l₂ ≡⟨ ++-assoc s₁ (s₂ ++ s₃) l₂ ⟩
           s₁ ++ ((s₂ ++ s₃) ++ l₂) ≡⟨ cong (s₁ ++_) (++-assoc s₂ s₃ l₂) ⟩
           s₁ ++ s₂ ++ s₃ ++ l₂ ∎
      ι' : (length s₁ + length s₂) ≤ (pumpingConstant re₁ + pumpingConstant re₂)
      ι' = ≤-tran ι (≤-+-right ≤-refl)
      α : (n : ℕ) →  (s₁ ++ napp n s₂ ++ s₃) ++ l₂ ≡ s₁ ++ napp n s₂ ++ s₃ ++ l₂
      α n = (s₁ ++ napp n s₂ ++ s₃) ++ l₂ ≡⟨ ++-assoc s₁ (napp n s₂ ++ s₃) l₂ ⟩
            s₁ ++ (napp n s₂ ++ s₃) ++ l₂ ≡⟨ cong (s₁ ++_) (++-assoc (napp n s₂) s₃ l₂) ⟩
            s₁ ++ napp n s₂ ++ s₃ ++ l₂ ∎
      g : (n : ℕ) → (s₁ ++ napp n s₂ ++ s₃ ++ l₂) =~ App re₁ re₂
      g n = subst (_=~ App re₁ re₂) (α n) (MApp (s₁ ++ napp n s₂ ++ s₃) l₂ re₁ re₂ (f n) m₂)
pumpingLemma .(l₁ ++ l₂) .(App re₁ re₂) (MApp l₁ l₂ re₁ re₂ m₁ m₂) p | inj₂ (r , q₂) with pumpingLemma l₂ re₂ m₂ q₂
pumpingLemma .(l₁ ++ l₂) .(App re₁ re₂) (MApp l₁ l₂ re₁ re₂ m₁ m₂) p | inj₂ (r , q₂) | s₁ , s₂ , s₃ , η , u , ι , f
  = l₁ ++ s₁ , s₂ , s₃ , η' , u , ι' , g
    where
      η' : l₁ ++ l₂ ≡ (l₁ ++ s₁) ++ s₂ ++ s₃
      η' = l₁ ++ l₂ ≡⟨ cong (l₁ ++_) η ⟩
           l₁ ++ s₁ ++ s₂ ++ s₃ ≡⟨ sym (++-assoc l₁ s₁ (s₂ ++ s₃))  ⟩
            (l₁ ++ s₁) ++ s₂ ++ s₃ ∎
      ι₃ : (length l₁ + length (s₁ ++ s₂)) ≡ (length (l₁ ++ s₁) + length s₂)
      ι₃ =  (length l₁ + length (s₁ ++ s₂)) ≡⟨ cong (length l₁ +_) (length-++ s₁ s₂) ⟩
           (length l₁ + (length s₁ + length s₂)) ≡⟨ +-assoc  (length l₁) (length s₁) (length s₂) ⟩
           length l₁ + length s₁ + length s₂ ≡⟨ cong (_+ length s₂) (sym (length-++ l₁ s₁)) ⟩
           length (l₁ ++ s₁) + length s₂ ∎
      ι₂ : length (s₁ ++ s₂) ≤ pumpingConstant re₂
      ι₂ = subst (_≤ pumpingConstant re₂) (sym (length-++ s₁ s₂)) ι
      ι₁ : (length l₁ + length (s₁ ++ s₂)) ≤ (pumpingConstant re₁ + pumpingConstant re₂)
      ι₁ = +-≤-both (≤-tran ≤-s r) ι₂
      ι' : (length (l₁ ++ s₁) + length s₂) ≤ (pumpingConstant re₁ + pumpingConstant re₂)
      ι' = subst (_≤ (pumpingConstant re₁ + pumpingConstant re₂)) ι₃ ι₁
      α : (n : ℕ) → l₁ ++ s₁ ++ napp n s₂ ++ s₃ ≡ (l₁ ++ s₁) ++ napp n s₂ ++ s₃
      α n = l₁ ++ s₁ ++ napp n s₂ ++ s₃ ≡⟨ sym (++-assoc l₁ s₁ (napp n s₂ ++ s₃)) ⟩
            (l₁ ++ s₁) ++ napp n s₂ ++ s₃ ∎
      g : (n : ℕ) → ((l₁ ++ s₁) ++ napp n s₂ ++ s₃) =~ App re₁ re₂
      g n = subst (_=~ App re₁ re₂) (α n) (MApp l₁ (s₁ ++ napp n s₂ ++ s₃) re₁ re₂ m₁ (f n))
pumpingLemma l .(Union re₁ re₂) (MUnionL .l re₁ re₂ m) p with bound-args-⊔ {pumpingConstant re₁} {pumpingConstant re₂} {length l} p
pumpingLemma l .(Union re₁ re₂) (MUnionL .l re₁ re₂ m) p | p' , _ with pumpingLemma l re₁ m p'
pumpingLemma l .(Union re₁ re₂) (MUnionL .l re₁ re₂ m) p | p' , _ | s₁ , s₂ , s₃ , η , u , ι , f
  = s₁ , s₂ , s₃ , η , u , ι' , g
  where
    ι' : length s₁ + length s₂ ≤ (pumpingConstant re₁ ⊔ pumpingConstant re₂)
    ι' = ≤-tran ι ≤-⊔-left
    g : (n : ℕ) → ExpMatch (s₁ ++ napp n s₂ ++ s₃) (Union re₁ re₂)
    g n = MUnionL (s₁ ++ napp n s₂ ++ s₃) re₁ re₂ (f n)
pumpingLemma l .(Union re₁ re₂) (MUnionR .l re₁ re₂ m) p with bound-args-⊔ {pumpingConstant re₁} {pumpingConstant re₂} {length l} p
pumpingLemma l .(Union re₁ re₂) (MUnionR .l re₁ re₂ m) p | _ , p' with pumpingLemma l re₂ m p'
pumpingLemma l .(Union re₁ re₂) (MUnionR .l re₁ re₂ m) p | _ , p' | s₁ , s₂ , s₃ , η , u , ι , f
  = s₁ , s₂ , s₃ , η , u , ι' , g
    where
      ι' : length s₁ + length s₂ ≤ (pumpingConstant re₁ ⊔ pumpingConstant re₂)
      ι' = ≤-tran ι ≤-⊔-right
      g : (n : ℕ) → ExpMatch (s₁ ++ napp n s₂ ++ s₃) (Union re₁ re₂)
      g n = MUnionR (s₁ ++ napp n s₂ ++ s₃) re₁ re₂ (f n)
pumpingLemma .[] .(Star re) (MStarZ re) p = ⊥! (snotz (q₁ ∙ q₂))
  where
    q₁ : 1 ≡ flat-ℕ (pumpingConstant re)
    q₁ = sym (pcLemma re)
    q₂ : flat-ℕ (pumpingConstant re) ≡ 0
    q₂ = cong flat-ℕ (≤-z-is-z p)
pumpingLemma .([] ++ l₂) .(Star re) (MStarS [] l₂ re m₁ m₂) p = pumpingLemma l₂ (Star re) m₂ p
pumpingLemma .((x ∷ l₁) ++ l₂) .(Star re) (MStarS (x ∷ l₁) l₂ re m₁ m₂) p with ≤-split (pumpingConstant re) (length (x ∷ l₁))
pumpingLemma .((x ∷ l₁) ++ l₂) .(Star re) (MStarS (x ∷ l₁) l₂ re m₁ m₂) p | inj₁ p' with pumpingLemma (x ∷ l₁) re m₁ p'
{- Can reuse iota because pumpingConstant of (Star re) is the same as the pumpingConstant of re -}
pumpingLemma .((x ∷ l₁) ++ l₂) .(Star re) (MStarS (x ∷ l₁) l₂ re m₁ m₂) p | inj₁ p' | s₁ , s₂ , s₃ , η , u , ι , f
  = s₁ , s₂ , s₃ ++ l₂ , η' , u , ι , g
    where
      η' : x ∷ l₁ ++ l₂ ≡ s₁ ++ s₂ ++ s₃ ++ l₂
      η' = (x ∷ l₁) ++ l₂ ≡⟨ cong (_++ l₂) η ⟩
           (s₁ ++ s₂ ++ s₃) ++ l₂ ≡⟨ ++-assoc s₁ (s₂ ++ s₃) l₂ ⟩
           s₁ ++ (s₂ ++ s₃) ++ l₂ ≡⟨ cong (s₁ ++_) (++-assoc s₂ s₃ l₂) ⟩
           s₁ ++ s₂ ++ s₃ ++ l₂ ∎
      α : (n : ℕ) → (s₁ ++ napp n s₂ ++ s₃) ++ l₂ ≡ s₁ ++ napp n s₂ ++ s₃ ++ l₂
      α n = (s₁ ++ napp n s₂ ++ s₃) ++ l₂ ≡⟨ ++-assoc s₁ (napp n s₂ ++ s₃) l₂ ⟩
            s₁ ++ (napp n s₂ ++ s₃) ++ l₂ ≡⟨ cong (s₁ ++_) (++-assoc (napp n s₂) s₃ l₂) ⟩
            s₁ ++ napp n s₂ ++ s₃ ++ l₂ ∎
      g : (n : ℕ) → (s₁ ++ napp n s₂ ++ s₃ ++ l₂) =~ Star re
      g n = subst (_=~ Star re) (α n) (MStarS (s₁ ++ napp n s₂ ++ s₃) l₂ re (f n) m₂)
pumpingLemma .((x ∷ l₁) ++ l₂) .(Star re) (MStarS (x ∷ l₁) l₂ re m₁ m₂) p | inj₂ q =
  [] , x ∷ l₁ , l₂ , refl , u , ≤-tran ≤-s q , f
    where
       u : (x ∷ l₁) ≢ []
       u p = snotz (cong length p)
       ι : (n : ℕ) → (x ∷ l₁) ++ napp n (x ∷ l₁) ++ l₂ ≡ napp (suc n) (x ∷ l₁) ++ l₂
       ι n = sym (++-assoc (x ∷ l₁) (napp n (x ∷ l₁)) l₂)
       f : (n : ℕ) → ([] ++ napp n (x ∷ l₁) ++ l₂) =~ Star re
       f zero = m₂
       f (suc n) = subst (_=~ Star re) (ι n) (MStarS (x ∷ l₁) (napp n (x ∷ l₁) ++ l₂) re m₁ (f n))


DecEq : Set ℓ → Set ℓ
DecEq A = (x y : A) → Dec (x ≡ y)

matchesEmpty : {A : Set ℓ} → RegExp A → Bool
matchesEmpty EmptySet = false
matchesEmpty EmptyStr = true
matchesEmpty (Char x) = false
matchesEmpty (App r s) = matchesEmpty r ∧ matchesEmpty s
matchesEmpty (Union r s) = matchesEmpty r ∨ matchesEmpty s
matchesEmpty (Star r) = true

nulled : {A : Set ℓ} → RegExp A → RegExp A
nulled EmptySet = EmptySet
nulled EmptyStr = EmptyStr
nulled (Char x) = EmptySet
nulled (App r s) = App (nulled r) (nulled s)
nulled (Union r s) = Union (nulled r) (nulled s)
nulled (Star r) = EmptyStr

nulledMatchIsEmpty : {A : Set ℓ} → (l : List A) → (r : RegExp A) → l =~ (nulled r) → l ≡ []
nulledMatchIsEmpty .[] EmptyStr MEmptyStr = refl
nulledMatchIsEmpty .(l₁ ++ l₂) (App r s) (MApp l₁ l₂ .(nulled r) .(nulled s) m₁ m₂) with nulledMatchIsEmpty l₁ r m₁ | nulledMatchIsEmpty l₂ s m₂
... | p | q = l₁ ++ l₂ ≡⟨ cong (_++ l₂) p ⟩
              l₂ ≡⟨ q ⟩ [] ∎
nulledMatchIsEmpty l (Union r s) (MUnionL .l .(nulled r) .(nulled s) m) = nulledMatchIsEmpty l r m
nulledMatchIsEmpty l (Union r s) (MUnionR .l .(nulled r) .(nulled s) m) = nulledMatchIsEmpty l s m
nulledMatchIsEmpty .[] (Star r) MEmptyStr = refl

nullMatchIsMatch : {A : Set ℓ} → (l : List A) (r : RegExp A) → l =~ (nulled r) → l =~ r
nullMatchIsMatch .[] EmptyStr MEmptyStr = MEmptyStr
nullMatchIsMatch .(l₁ ++ l₂) (App r s) (MApp l₁ l₂ .(nulled r) .(nulled s) m₁ m₂) with nullMatchIsMatch l₁ r m₁ | nullMatchIsMatch l₂ s m₂
... | p | q = MApp l₁ l₂ r s p q
nullMatchIsMatch l (Union r s) (MUnionL .l .(nulled r) .(nulled s) m) = MUnionL l r s (nullMatchIsMatch l r m)
nullMatchIsMatch l (Union r s) (MUnionR .l .(nulled r) .(nulled s) m) = MUnionR l r s (nullMatchIsMatch l s m)
nullMatchIsMatch .[] (Star r) MEmptyStr = MStarZ r

fnott : false ≢ true
fnott q = snotz (cong f q)
  where
    f : Bool → ℕ
    f false = 1
    f true = 0

breakAnd : (x y : Bool) → x ∧ y ≡ true → x ≡ true × y ≡ true
breakAnd false y p = ⊥! (fnott p)
breakAnd true y p = refl , p


breakOr : (x y : Bool) → x ∨ y ≡ true → x ≡ true ⊎ y ≡ true
breakOr false y p = inj₂ p
breakOr true y p = inj₁ refl

leftOrT : (x y : Bool) → x ≡ true → x ∨ y ≡ true
leftOrT false y p = ⊥! (fnott p)
leftOrT true _ _ = refl

rightOrT : (x y : Bool) → y ≡ true → x ∨ y ≡ true
rightOrT false _ p = p
rightOrT true _ _ = refl

matchEmptyImpliesNulled : {A : Set ℓ} → (r : RegExp A) → matchesEmpty r ≡ true → matchesEmpty (nulled r) ≡ true
matchEmptyImpliesNulled EmptySet p = ⊥! (fnott p)
matchEmptyImpliesNulled EmptyStr p = refl
matchEmptyImpliesNulled (Char x) p = ⊥! (fnott p)
matchEmptyImpliesNulled (App r s) p with breakAnd (matchesEmpty r) (matchesEmpty s) p
... | p₁ , p₂ with matchEmptyImpliesNulled r p₁ | matchEmptyImpliesNulled s p₂ 
... | q₁ | q₂  = matchesEmpty (nulled r) ∧ matchesEmpty (nulled s) ≡⟨ cong (_∧ matchesEmpty (nulled s)) q₁ ⟩
                 true ∧ matchesEmpty (nulled s) ≡⟨ q₂ ⟩
                 true ∎
matchEmptyImpliesNulled (Union r s) p with breakOr (matchesEmpty r) (matchesEmpty s) p
matchEmptyImpliesNulled (Union r s) p | inj₁ p' with (matchEmptyImpliesNulled r p')
... | q = leftOrT (matchesEmpty (nulled r)) (matchesEmpty (nulled s)) q
matchEmptyImpliesNulled (Union r s) p | inj₂ p' with (matchEmptyImpliesNulled s p')
... | q = rightOrT (matchesEmpty (nulled r)) (matchesEmpty (nulled s)) q
matchEmptyImpliesNulled (Star r) p = refl

regDeriv : {A : Set ℓ} → (eq : DecEq A) → A → RegExp A → RegExp A
regDeriv _ _ EmptySet = EmptySet
regDeriv _ _ EmptyStr = EmptySet
regDeriv eq a (Char b) with eq a b
regDeriv eq a (Char b) | yes p = EmptyStr
regDeriv eq a (Char b) | no ¬p = EmptySet
regDeriv eq a (App r s) = Union (App (regDeriv eq a r) s) (App (nulled r) (regDeriv eq a s))
regDeriv eq a (Union r s) = Union (regDeriv eq a r) (regDeriv eq a s)
regDeriv eq a (Star r) = App (regDeriv eq a r) (Star r)

recognizer : {A : Set ℓ} → (eq : DecEq A) → List A → RegExp A → Bool
recognizer eq [] r = matchesEmpty r
recognizer eq (a ∷ l) r = recognizer eq l (regDeriv eq a r)

charDeriv : {A : Set ℓ} → (eq : DecEq A) → (a : A) → regDeriv eq a (Char a) ≡ EmptyStr
charDeriv eq a with eq a a
charDeriv eq a | yes p = refl
charDeriv eq a | no ¬p = ⊥! (¬p refl)

matchTail : {A : Set ℓ} → (eq : DecEq A) → (a : A) → (l : List A) → (re : RegExp A)
          → l =~ (regDeriv eq a re) → (a ∷ l) =~ re
matchTail eq a l (Char b) m with eq a b
matchTail eq a [] (Char b) m | yes p = subst (λ z → (a ∷ []) =~ Char z) p (MChar a)
matchTail eq a .(l₁ ++ l₂) (App r s) (MUnionL .(l₁ ++ l₂) .(App (regDeriv eq a r) s) .(App (nulled r) (regDeriv eq a s)) (MApp l₁ l₂ .(regDeriv eq a r) .s m₁ m₂))
  = MApp (a ∷ l₁) l₂ r s (matchTail eq a l₁ r m₁) m₂
matchTail eq a .(l₁ ++ l₂) (App r s) (MUnionR .(l₁ ++ l₂) .(App (regDeriv eq a r) s) .(App (nulled r) (regDeriv eq a s)) (MApp l₁ l₂ .(nulled r) .(regDeriv eq a s) m₁ m₂))
  = subst (λ z → (a ∷ z ++ l₂) =~ App r s) (sym η) (MApp [] (a ∷ l₂) r s η' (matchTail eq a l₂ s m₂) )
  where
    η : l₁ ≡ []
    η = nulledMatchIsEmpty l₁ r m₁
    η' : [] =~ r
    η' = subst (_=~ r) η (nullMatchIsMatch l₁ r m₁)
matchTail eq a l (Union r s) (MUnionL .l .(regDeriv eq a r) .(regDeriv eq a s) m) = MUnionL (a ∷ l) r s (matchTail eq a l r m)
matchTail eq a l (Union r s) (MUnionR .l .(regDeriv eq a r) .(regDeriv eq a s) m) = MUnionR (a ∷ l) r s (matchTail eq a l s m)
matchTail eq a .(l₁ ++ l₂) (Star r) (MApp l₁ l₂ .(regDeriv eq a r) .(Star r) m₁ m₂) =
  MStarS (a ∷ l₁) l₂ r (matchTail eq a l₁ r m₁) m₂

recToMatchEmpty : {A : Set ℓ} → (r : RegExp A) → matchesEmpty r ≡ true → [] =~ (nulled r)
recToMatchEmpty EmptySet p = ⊥! (fnott p)
recToMatchEmpty EmptyStr p = MEmptyStr
recToMatchEmpty (Char a) p = ⊥! (fnott p)
recToMatchEmpty (App r s) p with breakAnd (matchesEmpty r) (matchesEmpty s) p
recToMatchEmpty (App r s) p | q₁ , q₂ with recToMatchEmpty r q₁ | recToMatchEmpty s q₂
... | qr | qs = MApp [] [] (nulled r) (nulled s) qr qs
recToMatchEmpty (Union r s) p with breakOr (matchesEmpty r) (matchesEmpty s) p
... | inj₁ q = MUnionL [] (nulled r) (nulled s) (recToMatchEmpty r q)
... | inj₂ q = MUnionR [] (nulled r) (nulled s) (recToMatchEmpty s q)
recToMatchEmpty (Star r) p = subst ([] =~_) (sym η) MEmptyStr
  where
    η : nulled (Star r) ≡ EmptyStr
    η = refl 


recToMatchT : {A : Set ℓ} → (eq : DecEq A) → (l : List A) → (r : RegExp A) → recognizer eq l r ≡ true → l =~ r
recToMatchT eq [] r p = nullMatchIsMatch [] r (recToMatchEmpty r p)
recToMatchT eq (a ∷ l) r p = matchTail eq a l r (recToMatchT eq l (regDeriv eq a r) p)

{- Old Docs Ex
https://agda.readthedocs.io/en/v2.6.1/language/with-abstraction.html#the-inspect-idiom -}
data Singleton {a} {A : Set a} (x : A) : Set a where
  _with≡_ : (y : A) → x ≡ y → Singleton x

inspect : ∀ {a} {A : Set a} (x : A) → Singleton x
inspect x = x with≡ refl

recChar : {A : Set ℓ} → (eq : DecEq A) → (a : A) → recognizer eq (a ∷ []) (Char a) ≡ true
recChar eq a with eq a a
... | yes p = refl
... | no ¬p = ⊥! (¬p refl)

recUnionL : {A : Set ℓ} → (eq : DecEq A) → (l : List A) → (r s : RegExp A)
  → recognizer eq l r ≡ true → recognizer eq l (Union r s) ≡ true
recUnionL eq [] r s p = matchesEmpty r ∨ matchesEmpty s ≡⟨ cong (_∨ matchesEmpty s) p ⟩
                        true ∎ 
recUnionL eq (a ∷ l) r s p = recUnionL eq l (regDeriv eq a r) (regDeriv eq a s) p

recUnionR : {A : Set ℓ} → (eq : DecEq A) → (l : List A) → (r s : RegExp A)
  → recognizer eq l s ≡ true → recognizer eq l (Union r s) ≡ true
recUnionR eq [] r s p with inspect (matchesEmpty r)
... | false with≡ q = matchesEmpty r ∨ matchesEmpty s ≡⟨ cong (_∨ matchesEmpty s) q ⟩
                      matchesEmpty s ≡⟨ p ⟩
                      true ∎
... | true with≡ q = recUnionL eq [] r s q
recUnionR eq (a ∷ l) r s p = recUnionR eq l (regDeriv eq a r) (regDeriv eq a s) p

prodStar : {A : Set ℓ} → (l : List A) → (r : RegExp A) →  l =~ Star r → (l ≢ [])
  → Σ[ l₁ ∈ List A ] Σ[ l₂ ∈ List A ] (l ≡ l₁ ++ l₂ × (l₁ ≢ []) × (l₁ =~ r) × (l₂ =~ Star r))
prodStar .[] r (MStarZ .r) u = ⊥! (u refl)
prodStar .([] ++ l₂) r (MStarS [] l₂ .r m₁ m₂) u = prodStar l₂ r m₂ u
prodStar .((a ∷ l₁) ++ l₂) r (MStarS (a ∷ l₁) l₂ .r m₁ m₂) u
  = a ∷ l₁ , l₂ , refl , v , m₁ , m₂
  where
    v : (a ∷ l₁) ≢ []
    v p = snotz (cong length p)
matchToRec : {A : Set ℓ} → (eq : DecEq A) → (l : List A) → (r : RegExp A) → l =~ r → recognizer eq l r ≡ true
recApp : {A : Set ℓ} → (eq : DecEq A) → (l₁ l₂ : List A) → (r s : RegExp A)
  → recognizer eq l₁ r ≡ true → recognizer eq l₂ s ≡ true → recognizer eq (l₁ ++ l₂) (App r s) ≡ true

recApp eq [] [] r s p₁ p₂ = matchesEmpty r ∧ matchesEmpty s ≡⟨ cong (_∧ matchesEmpty s) p₁ ⟩
                            true ∧ matchesEmpty s ≡⟨ p₂ ⟩
                            true ∎ 
recApp eq [] (a ∷ l₂) r s p₁ p₂ with recApp eq [] l₂ (nulled r) (regDeriv eq a s) (matchEmptyImpliesNulled r p₁) p₂
... | q  = recUnionR eq ([] ++ l₂) (App (regDeriv eq a r) s) (App (nulled r) (regDeriv eq a s)) q 
recApp eq (a ∷ l₁) l₂ r s p₁ p₂ with recApp eq l₁ l₂ (regDeriv eq a r) s p₁ p₂
... | q = recUnionL eq (l₁ ++ l₂) (App (regDeriv eq a r) s) (App (nulled r) (regDeriv eq a s)) q

matchToRec eq .[] .EmptyStr MEmptyStr = refl
matchToRec eq .(a ∷ []) .(Char a) (MChar a) = recChar eq a
matchToRec eq .(l₁ ++ l₂) .(App r s) (MApp l₁ l₂ r s m₁ m₂) = recApp eq l₁ l₂ r s (matchToRec eq l₁ r m₁) (matchToRec eq l₂ s m₂)
matchToRec eq l .(Union r s) (MUnionL .l r s m) = recUnionL eq l r s (matchToRec eq l r m)
matchToRec eq l .(Union r s) (MUnionR .l r s m) = recUnionR eq l r s (matchToRec eq l s m)
matchToRec eq .[] .(Star r) (MStarZ r) = refl
matchToRec eq .([] ++ l₂) .(Star r) (MStarS [] l₂ r m₁ m₂) = matchToRec eq l₂ (Star r) m₂
matchToRec eq .((a ∷ l₁) ++ l₂) .(Star r) (MStarS (a ∷ l₁) l₂ r m₁ m₂) =
  recApp eq l₁ l₂ (regDeriv eq a r) (Star r) (matchToRec eq (a ∷ l₁) r m₁) (matchToRec eq l₂ (Star r) m₂)
