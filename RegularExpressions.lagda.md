# Verified Regular Expressions in Agda (revisited) - Part 1

A few years ago, I tried proving the pumping lemma as a small

project to motivate myself to learn
[Agda](https://wiki.portal.chalmers.se/agda/pmwiki.php).
I have a terrible habit of setting up blogs only to dismantle
them soon after.
So I thought I'd expand what I had written previously
for this current cycle.
In addition to (re)proving the pumping lemma, I thought I'd
also implement a boolean function to recognize if a string
matches a regular expression based on the
[Brzozowski derivative](https://en.wikipedia.org/wiki/Brzozowski_derivative),
and then (this being a post written in a depedently typed lanague) verify
this function is correct.
I also thought I'd make this code a little more idiomatic and to use more of
Agda's features. 

## Ambience

```agda
{-# OPTIONS --cubical #-}
```
The choice of using the cubical agda library is philosophical, not practical.
A large part of the reason I initially began to fool around with depedently-typed
languages was I became interested in [Univalent Foundations](https://en.wikipedia.org/wiki/Univalent_foundations)
and formal verification.
As an aside, I use the term univalence instead of [homotopy type theory](https://homotopytypetheory.org/book/)
because I think it's cooler (though because I'm a child, I did think it was funny that the mailing list was once called
"hott-amateurs").
I bought into the selling points of univalent foundations and have some personal opinions on mathematics as field of
human study which I think are friendly to univalent foundations
1. Machine checkable proofs; 
2. Proofs are normal objects of the language; not terms of the meta-language
3. The univalence axiom is sexy; an isomoprhism is an identification
Now this might be my most controversial opinion ("hott-take"):
I think proof-relevant mathematics formalizes at least some notions of aesthetics within
mathematics. An assumption which is not needed becomes a function arugment which can be elimanated. Or a
corollary is a one-line function which simply calls a another more general function.
Or conversely, instead of using a cannon to kill a mosquito we have an elegant direct proof.
Some of this is shunting the problem of aesthetics of mathematics to the aesthetic of programming but I
believe there is something about this idea even if my I'm poor evangelist, what with
my lack of philosophical training and overall idiocy.
And if univalent foundations should be trusted as setting in which to reason about important things like
group theory, then it should be an acceptable setting to reason about grubby mundane trivial pedestrian
things like software.
So this post is also a small experiement to test the ergonomics of verifying software in univalent setting.

Now the flag isn't `--univalence' or `--hott`, it's `--cubical`. 
Cubical type theory is an attempt to give the univalence computational content. 
The univalence axiom is actually a theory in the cubical setting which is nice because
the univalence axiom implies the existence of a function from isomorphisms between two types
to identities between two types and cubical type theory constructs that function.

## Imports
With our compiler flag set we then begin the normal preamble where we declare our module
and import what we need to from prelude and standard library
```agda
module RegularExpressions where
```
Because this post is pedagogical in nature there is a lot that we don't import.
My rule for this post is generally to import functions and data types but to
re-implement proofs since I think people have a seen a list or booleans defined
many more times than they have seen a proof that appending lists is associative.

So first we import the univalent notion of equality and a toolset with which
to reason about equations.
```agda
open import Cubical.Foundations.Everything
  using (_≡_
        ; refl
        ; cong
        ; sym
        ; _∙_
        ; subst
        ; step-≡
        ; _∎
        ; funExt
        ; Level
        ; ℓ-max
        ; ℓ-suc
        ; _≃_
        ; isEquiv
        ; equiv-proof
        ; isContr
        ; fiber)
```

The empty type `⊥` corresponds to the logical notion of falsity.
If you have taken introductory philosophical or rhetorical course in logic,
along with learning Socrates was mortal, you might have learned the
"principle of explosion" or "ex falso quodlibet": any logical statement
follows from a contradiction
(the [Coq tactic](https://coq.inria.fr/library/Coq.Init.Tactics.html) which corresponds to this principle
is literraly called `exfalso`).
Now Agda does have very robust depedent pattern matching.
A lot of times Agda is able to reason on its
own that some combinations of arguments will never occur or is absurd.
The simplest example is if you define the head of a vector with positive length
you don't need to consider what the head of an empty vector is.
You can exclude that case from the function definition or Agda will mark `head []`
as absurd and Agda and you are comfortable that will never ever see `head []` in
any program.
But of course, Agda can not reason about every absurdity.
I'm pretty sure that's the just weak form of Rice's theorem: nothing interesting is
decideable.
In these instances we help Agda: first by constructing some term of type `⊥`
and then calling the function `⊥!` which is a function from `⊥` to any type we wish.
The library denotes this function `rec` since it is the "recursion principle" for
the empty type.
I think `⊥!` is more evocative as the unique function from `⊥` to any type `A` but
this is purely a matter of taste. 

In cubical setting we can't always pattern match on equality, and instead
we often need to use the path operators we imported above.
So our choice to use `--cubical` will force us to rely on constructing contradictions more
than if we had used Agda's default settings.
But I'm pretty sure needlessly suffering for one's philosophical rigidity is a virtue.

```agda
open import Cubical.Data.Empty
  renaming (rec to ⊥!)
```

Now that we have a notion of falsity, we have a notion of negation. 
For any type `A`, the negation `¬ A` is the type of functions `A → ⊥`.
Something is considered false if we can prove a contradiction from it.
Now a term of type `(Dec A)` is returns `yes` with a proof that type `A` is true/inhabited (the proof simply an element of `A`)
or `no` with a proof `A → ⊥`, which is a function with precisely that type. 
What we will later see, which I think is interesting, is that while we don't need this notion of decideability
(or the related notion of decideable equality) to prove the pumpingLemma we will need it write a decision procedure
for matching strings to regular expressions.

```agda
open import Cubical.Relation.Nullary
  using (Dec ; no ; yes ;  ¬_ )
```
The next few imports are straight-forward.
We import lists, natural numbers, booleans, their constructors and various function on these types.

```agda
open import Data.List as List
  using (List ; _∷_ ; [] ; _++_ ; length)

open import Data.Nat
  using (ℕ ; suc ; zero ; _+_ ; _⊔_)

open import Data.Bool
  using (Bool ; true ; false ; _∧_ ; _∨_ ; not)
```

Next import dependent products, non-dependent products, and sums.
This corresponds to the logical notions "exists", "and", "or".
Interseting to note, `∃ x ∈ A, B` is the same `A and B` when the proposition `B`
is not a function of `x`.
Similarly, `∀ x ∈ A , B` is the same as `A → B` when `B` is not a function of `x`.
We don't need to import the latter because the notion of functions (dependent or not)
are foundational to Agda.
I think it could be an interesting experiment to create a language where functions are
defined as some inductive type with `λ` as the constructor.
But I half-suspect there's some smalltalk-esque language that already does that. 

```agda
open import Data.Product
  using (_×_  ; Σ-syntax ; _,_)

open import Data.Sum
  using (_⊎_ ; inj₁ ; inj₂)
```
We've talked about proving from contradictions, and the function snotz is going to be
a building to actual construct contradictions.
It's simple a function from `(suc n ≡ 0) → ⊥` (which we also write as `suc n ≢ 0`).
So if can ever provide a proof that a positive natural is equal to zero, then
`snotz` gives us a term of type `⊥` which we then use to prove whatever we want using
`⊥!`.

```agda
open import Cubical.Data.Nat
  using (snotz)
```

Agda let's us declare variables that we can then use throughout the module.
We will have a lot of functions parameterized over a type `A` and it is
convenient to write `f : (A : Set ℓ) → A → List A` instead of having to add the
preamble
`f : {ℓ : Level} (A : Set ℓ) → A → List A`
to every function definition.
I think everyone would be comfortable with the type of `A` being some sort of set,
but `ℓ` might need more explaining.
If the type of `A` is `Set` then what is the type of `Set`?
If we have `Set : Set` then we have a problem.
We have a type-theorhetic analog to
[Russell's paradox](https://plato.stanford.edu/entries/russell-paradox/)
called Girard's paradox.
The resolution is to have a hierarchy of type, so if `A : Set₀`
then `Set₀ : Set₁`, `Set₁ : Set₂`, and so on.
Now for same reasons we want to paramertirize over types,
we want to pramerterize over universe levels.
I don't to have to define a list of numbers, a list of booleans, a list of strings,
I want to define a list once.
And I don't have to define a list of `Set₀` types, a list of `Set₁` types, a
list of `Set₂` types, etc.
So we parameterize over universe levels.
I would like to be able to use the same trick to not have to explicitly write
`(A : Set ℓ)` but I'm a dummy who can only think of the naïve way to do it
which doesn't work and I have trouble parsing the error message:
"The sort of ___ cannot depend on its indices in the type".
We do what we can.
```agda
variable
  ℓ : Level
```

We're finally ready to begin and define the type of regular expressions.
Agda is very liberal with what characters can be used as identifiers, and has
powerful mechanism for defining mixfix operators.
So we write the connectives for regular expressions pretty close to the notation
we would usually see when reading a paper or book.
Though when we could confuse one of these constructors for a different operator
I've added a subscript `r` to remind us of the setting.

```agda
data RegExp (A : Set ℓ) : Set ℓ where
  ∅ ϵ : RegExp A
  [_]ᵣ : A → RegExp A
  _+ᵣ_ _∪ᵣ_ : RegExp A → RegExp A → RegExp A
  _* : RegExp A → RegExp A
```

The match data type provides a direct proof that a string (list of something)
matches a regular expression instead of delegating the proof to some predicate
and seeing if  `matches? string regex ≡ true`.
It provides the semantics of regular expressions, since how we match is
what makes `_+ᵣ_` different from `_∪ᵣ_`.

```agda
data ExpMatch {A : Set ℓ} : List A → RegExp A → Set ℓ where
  Mϵ : ExpMatch [] ϵ
  M[] : (x : A) → ExpMatch (x ∷ []) [ x ]ᵣ
  M+ : (xs ys : List A) (r s : RegExp A)
       → ExpMatch xs r → ExpMatch ys s 
       → ExpMatch (xs ++ ys) (r +ᵣ s)
  M∪ₗ : (xs : List A) (r s : RegExp A)
    → ExpMatch xs r → ExpMatch xs (r ∪ᵣ s)
  M∪ᵣ : (xs : List A) (r s : RegExp A)
    → ExpMatch xs s → ExpMatch xs (r ∪ᵣ s)
  M*Z : (r : RegExp A) → ExpMatch [] (r *)
  M*S : (xs ys : List A) (r : RegExp A)
    → ExpMatch xs r → ExpMatch ys (r *)
    → ExpMatch (xs ++ ys) (r *)
```
Not there is no constructor for matching the emptyset `∅` since there is no
list which should match this regular expression.

So one limitation I should not is that we do not have a notion of the
complement/negation of a regular expression. 
It's a hard thing to bake in to the inductive definition of ExpMatch. 
I feel a constructor of the type
`MNot : ¬ (l =~ r) → (l =~ (Not r)` but there's a problem.
Agda enforces strict positivity.
For a simpler example you can see
[this section](https://agda.readthedocs.io/en/v2.5.4/language/data-types.html?highlight=positivity#strict-positivity)
in the docs and this
[stack overflow] answer by Jesper Cockx giving more exposition.
If this positivity conidition wasn't enforced you could allow this definition
`
data ⊥ : Set where

data Bad : Set where
  bad : (Bad → ⊥) → Bad

self-app : Bad → ⊥
self-app (bad f) = f (bad f)

absurd : ⊥
absurd = self-app (bad self-app)
`
and actually having a term `absurd : ⊥` is, well, absurd.
But Agda rules this out and complains that in the constructor `bad` the first appearance
of `Bad` is in a negative position. 
The `bad` constructor has the same form as our hypothetical `MNot` constructor.

The negativity issue is the constructor's argument is a function whose domain is the
datatype i nquestion. It's perfectly fine to have a constructor whose argument is a function,
even a function whose range is the datatype.

This post is a literate Agda program.
You can run this post through the type checker and see that the following definitions illicit no
complaint from Agda. 
```agda
data Good₁ : Set where
  good : (ℕ → ⊥) → Good₁

data Good₂ : Set where
  good : (ℕ → Good₂) → Good₂
```
Now this issue can be resolved if we define a recognizer predicate as well and used the proof
the predicate `recognize l r ≡ true` as the argument for the constructor.
And I think we can also resolve this issue using
[sized type](https://agda.readthedocs.io/en/v2.6.3.20230805/language/sized-types.html),
and maybe I will do that alter but this is already a pretty lengthy posts.

Speaking of length, we introduce the notation `l =~ r` for brevity
and `≢` for the negation of equality

```agda
_=~_ : {A : Set ℓ} (l : List A) (re : RegExp A) → Set ℓ
l =~ re = ExpMatch l re

_≢_ : {A : Set ℓ} → A → A → Set ℓ
x ≢ y = ¬ (x ≡ y)
```
Instead of considering a list of characters or a list of numbers,
let's get a little weird.
Let's consider lists of functions `ℕ → ℕ` and regexes  on them.
So under the cubical assumption, function equaluity is existential.
Two functions `f,g` are equal if they are similar, so `f ≡ g` if
`f x ≡ g x` for all `x`s in the domain. 
Proving two functions are equal is trivial when their definitions coincide.
So we have
```agda
test₁ : (n : ℕ) → suc n ≡ 1 + n
test₁ n  = refl
```
since if we peer under the hood, `1 + n` is defined to be `suc n`.
Now since function equaility is existential, we also have the equality
```
suc' : ℕ → ℕ
suc' n = n + 1

test₂ : (n : ℕ) → suc n ≡ suc' n
test₂ zero = refl
test₂ (suc n) = cong suc (test₂ n)

```
Our proof is a bit more involed since these two functions aren't equal
intensionally.
`cong` is our first instance of equational reasoning, and has the
type `(f : A → B) → (a ≡ b) → (f a ≡ f b)`.
These functions have a different definitions, but we are able to show they coincide
and therefore are equal under cubical type theory.

```agda
test₃ : suc ≡ suc'
test₃ = funExt test₂
```
Let's also define an identity function on `ℕ` just so we have more functions to
use in our regular expressions.

```agda
idℕ : ℕ → ℕ
idℕ n = n
```

And let's try to prove a list of functions matches a regular expression.

```agda
m₁ : (idℕ ∷ []) =~ [ idℕ ]ᵣ
m₁ = M[] idℕ

m₂ : (suc ∷ []) =~ [ suc ]ᵣ
m₂ = M[] suc

m₃ : (suc' ∷ []) =~ [ suc' ]ᵣ
m₃ = M[] suc'

test₄ : ((λ x → x) ∷ suc' ∷ []) =~ ([ idℕ ]ᵣ +ᵣ [ suc ]ᵣ)
test₄ = subst B p (M+ (idℕ ∷ []) (suc ∷ []) [ idℕ ]ᵣ [ suc ]ᵣ (M[] idℕ) (M[] suc))
  where
    B : (ℕ → ℕ) → Set₀
    B f = ((λ x → x) ∷ f ∷ []) =~ ([ idℕ ]ᵣ +ᵣ [ suc ]ᵣ)
    p : suc ≡ suc'
    p = test₃
```
`subst` is our second pieve of equational reasoning, and its type is
`(f : A → Set) → a ≡ b → f a → f b`.
So if we have some property `f a` and know `a ≡ b` then we can prove `f b`. 
We have to use this since we have `suc'` in our string, but `suc` in our regex.
This has been a strange detour but I think it's one worth taking.
We have constructed regexes in such a way that we can ask and prove if some
string matches a regex even if we are operating over types which don't have
a decideable notion of equality.
We can ask `[ f ] =~ [ g ]ᵣ` where `f , g : ℕ → ℕ` are arbitrary functions.
But in general we should not be able to prove or disprove this statement since
in general we can't determine if two functions are equal.
But here's what's stranger, that's sometimes okay.
We don't need to have the ability to determine to functions are equal to
prove the pumping lemma holds on lists of functions.
However, we will need the ability to decidide equality between any two elements to
define a function which determines if a string matches a regex.
But we can ignore that for a surprisingly long time in this post.

First we need to define a pumping constant for a regular expression.
We can only "pump" a string if its longer than this number we associate to
each regular expression.

```agda
pc : {A : Set ℓ} → RegExp A → ℕ
pc ∅ = 1
pc ϵ = 1
pc [ x ]ᵣ = 2
pc (r +ᵣ s) = pc r + pc s
pc (r ∪ᵣ s) = pc r ⊔ pc s
pc (r *) = pc r
```
where `x ⊔ y` is the maximum of the two arguments.
We also going to need to prove some minor properties about naturals to use
in our proof of the pumping lemma.
We could have imported the datatype `x ≤ y` like our other datatypes but
this might be a useful exercise to define and reason about our implementation.
This way we get a little bit of practice proofs before dealing with the much
more unwieldy pumping lemma.


```agda
data _≤_ : ℕ → ℕ → Set where
  z≤n : {n : ℕ} → zero ≤ n
  s≤s : {m n : ℕ} → m ≤ n → suc m ≤ suc n

infix 4 _≤_

_<_ : (m n : ℕ) → Set
m < n = suc m ≤ n

infix 4  _<_
```
The `infix` declaration sets the operator precedence when parsing.
Now this is not the only definition we could have used.
The following is logically equivalent and we can prove it.
```
data _≤'_ : ℕ → ℕ → Set where
  n≤'n : {n : ℕ} → n ≤' n
  n≤'s : {m n : ℕ} → m ≤' n → m ≤' suc n

≤'s : {m n : ℕ} → m ≤' n → suc m ≤' suc n
≤'s n≤'n = n≤'n
≤'s (n≤'s p) = n≤'s (≤'s p)

≤s : {m n : ℕ} → m ≤ n → m ≤ suc n
≤s z≤n = z≤n
≤s (s≤s p) = s≤s (≤s p)

≤→≤' : (m n : ℕ) → m ≤ n → m ≤' n
≤→≤' zero zero _ = n≤'n
≤→≤' zero (suc n) _ = n≤'s (≤→≤' zero n z≤n)
≤→≤' (suc m) (suc n) (s≤s p) = ≤'s (≤→≤' m n p)

≤'→≤ : (m n : ℕ) → m ≤' n → m ≤ n
≤'→≤ zero n p = z≤n
≤'→≤ (suc m) (suc .m) n≤'n = s≤s (≤'→≤ m m n≤'n)
≤'→≤ (suc m) (suc n) (n≤'s p) = ≤s (≤'→≤ (suc m) n p)
```
But `≤` is easier to work with in practice.

Before we prove our result, we define it.
Record types in Agda equivalent to nested `Σ`s but if
we have a feature then lets use it.

We will need a lot of little results on natural and
numbers and lists to prove our main result.


```agda
+-z-r : (n : ℕ) → n + zero ≡ n
+-z-r zero = refl
+-z-r (suc n) = cong suc (+-z-r n)

+-s-r : (m n : ℕ) → m + suc n ≡ suc (m + n)
+-s-r zero n = refl
+-s-r (suc m) n = cong suc (+-s-r m n)

+-comm : (m n : ℕ) → n + m ≡ m + n
+-comm zero n = +-z-r n
+-comm (suc m) n = n + suc m ≡⟨ +-s-r n m ⟩
                   suc (n + m) ≡⟨ cong suc (+-comm m n) ⟩
                   suc (m + n) ∎

+-assoc : (a b c : ℕ) → a + (b + c) ≡ a + b + c
+-assoc zero b c = refl
+-assoc (suc a) b c = cong suc (+-assoc a b c)

≤-step : (n : ℕ) → n ≤ suc n
≤-step zero = z≤n
≤-step (suc n) = s≤s (≤-step n)

≤-+-r : (m n : ℕ) → m ≤ m + n
≤-+-r zero n = z≤n
≤-+-r (suc m) n = s≤s (≤-+-r m n)

≤-refl : {n : ℕ} → n ≤ n
≤-refl {zero} = z≤n
≤-refl {suc n} = s≤s ≤-refl

≤-tran : {a b c : ℕ} → a ≤ b → b ≤ c → a ≤ c
≤-tran z≤n z≤n = z≤n
≤-tran z≤n (s≤s q) = z≤n
≤-tran (s≤s p) (s≤s q) = s≤s (≤-tran p q)

≤-+-l : (m n : ℕ) → m ≤ n + m
≤-+-l m zero = ≤-refl
≤-+-l m (suc n) = ≤-tran (≤-+-l m n) (≤-step (n + m))

≤-z-is-z : {a : ℕ} → a ≤ zero → a ≡ zero
≤-z-is-z z≤n = refl

≤-di : (m n : ℕ) → m ≤ n ⊎ (n < m)
≤-di zero n = inj₁ z≤n
≤-di (suc m) zero = inj₂ (s≤s z≤n)
≤-di (suc m) (suc n) with ≤-di m n
... | inj₁ q = inj₁ (s≤s q)
... | inj₂ q = inj₂ (s≤s q)

+=z-split : (a b : ℕ) → a + b ≡ zero → a ≡ zero × b ≡ zero
+=z-split zero zero p = refl , refl
+=z-split zero (suc b) p = ⊥! (snotz p)
+=z-split (suc a) b p = ⊥! (snotz p)

+≤n-split : (a b c : ℕ) → a + b ≤ c → a ≤ c × b ≤ c
+≤n-split zero zero zero _ = z≤n , z≤n
+≤n-split zero b (suc c) p = z≤n , p
+≤n-split (suc a) zero (suc c) p = q , z≤n
  where
    q : suc a ≤ suc c
    q = subst (λ z → suc z ≤ suc c ) (+-z-r a) p
+≤n-split (suc a) (suc b) (suc c) (s≤s p) with +≤n-split a (suc b) c p
... | q₁ , q₂ = s≤s q₁ , ≤-tran q₂ (≤-step c)

+≤-split : (a b c d : ℕ) → a + b ≤ c + d → a ≤ c ⊎ c ≤ a × b ≤ d
+≤-split zero b c d p = inj₁ z≤n
+≤-split (suc a) b zero (suc d) (s≤s p) with +≤-split a b zero d p
... | inj₁ z≤n = inj₂ (z≤n , ≤-tran p (≤-step d))
... | inj₂ (q1 , q2) = inj₂ (z≤n , ≤-tran q2 (≤-step d))
+≤-split (suc a) b (suc c) d (s≤s p) with +≤-split a b c d p
... | inj₁ q = inj₁ (s≤s q)
... | inj₂ (q1 , q2) = inj₂ (s≤s q1 , q2)

+≤-mon : (a b c d : ℕ) → a ≤ c → b ≤ d → a + b ≤ c + d
+≤-mon .zero b c d z≤n q = ≤-tran q (≤-+-l d c)
+≤-mon .(suc _) b .(suc _) d (s≤s p) q = s≤s (+≤-mon _ b _ d p q)

⊔≤ : (a b c : ℕ) → a ⊔ b ≤ c → a ≤ c × b ≤ c
⊔≤ zero b c p = z≤n , p
⊔≤ (suc a) zero c p = p , z≤n
⊔≤ (suc a) (suc b) (suc c) (s≤s p) with ⊔≤ a b c p
... | q1 , q2 = s≤s q1 , s≤s q2

⊔-≤-r : ( m n : ℕ) → m ≤ m ⊔ n
⊔-≤-r zero n = z≤n
⊔-≤-r (suc m) zero = s≤s ≤-refl
⊔-≤-r (suc m) (suc n) = s≤s (⊔-≤-r m n)

⊔-≤-l : ( m n : ℕ) → m ≤ n ⊔ m
⊔-≤-l zero zero = z≤n
⊔-≤-l (suc m) zero = s≤s ≤-refl
⊔-≤-l zero (suc n) = z≤n
⊔-≤-l (suc m) (suc n) = s≤s (⊔-≤-l m n)

⊔=z-split : (m n : ℕ) → m ⊔ n ≡ zero → m ≡ zero × n ≡ zero
⊔=z-split zero zero p = refl , refl
⊔=z-split zero (suc n) p = ⊥! (snotz p)
⊔=z-split (suc m) zero p = ⊥! (snotz p)
⊔=z-split (suc m) (suc n) p = ⊥! (snotz p)

pc-pos : {A : Set ℓ} (r : RegExp A) → pc r ≢ zero
pc-pos ∅ p = snotz p
pc-pos ϵ p = snotz p
pc-pos [ x ]ᵣ p = snotz p
pc-pos (r +ᵣ s) p with +=z-split (pc r) (pc s) p
... | q , _ = ⊥! (pc-pos r q)
pc-pos (r ∪ᵣ s) p with ⊔=z-split (pc r) (pc s) p
... | q , _ = ⊥! (pc-pos r q)
pc-pos (r *) p = pc-pos r p

++-length : {A : Set ℓ} (xs ys : List A) → length (xs ++ ys) ≡ length xs + length ys
++-length [] ys = refl
++-length (x ∷ xs) ys = cong suc (++-length xs ys)
```
The above proof is so simple because of the inducive definition `_+_` and `_++_`.
We could have made life more difficult by defining addition recursively on the
second argument.
```
_+'_ : ℕ → ℕ → ℕ
m +' zero = m
m +' suc n = suc (m +' n)

++-length' : {A : Set ℓ} (xs ys : List A) → length (xs ++ ys) ≡ (length xs +' length ys)
++-length' [] [] = refl
++-length' [] (y ∷ ys) = suc (length ys) ≡⟨ cong suc (++-length' [] ys) ⟩
                         (suc (zero +' length ys)) ∎
++-length' (x ∷ xs) ys = suc (length (xs ++ ys)) ≡⟨ cong suc (++-length' xs ys) ⟩
                         suc (length xs +' length ys) ≡⟨ sym (+'-s-l (length xs) (length ys)) ⟩
                         (suc (length xs) +' length ys) ∎
  where
    +'-s-l : (m n : ℕ) → (suc m +' n) ≡ suc (m +' n)
    +'-s-l m zero = refl
    +'-s-l m (suc n) = cong suc (+'-s-l m n)

++-assoc : {A : Set ℓ} (xs ys zs : List A) → (xs ++ ys) ++ zs ≡ xs ++ ys ++ zs
++-assoc [] ys zs = refl
++-assoc (x ∷ xs) ys zs = cong (x ∷_) (++-assoc xs ys zs)

++-4-l : {A : Set ℓ} (as bs cs ds : List A) → (as ++ bs ++ cs) ++ ds ≡ as ++ bs ++ cs ++ ds
++-4-l as bs cs ds = (as ++ bs ++ cs) ++ ds ≡⟨ ++-assoc as (bs ++ cs) ds ⟩
                     as ++ (bs ++ cs) ++ ds ≡⟨ cong (as ++_) (++-assoc bs cs ds) ⟩
                     as ++ bs ++ cs ++ ds ∎

rep : {A : Set ℓ} → ℕ → List A → List A
rep zero l = []
rep (suc n) l = l ++ rep n l

```
Before we prove our result, we define it.
Record types in Agda equivalent to nested `Σ`s but if
we have a feature then lets use it.

```agda
record pumpingLemma {A : Set ℓ} (l : List A) (r : RegExp A) : Set ℓ where
--  constructor _,_,_,_,_,_,_
  field
    s1 : List A
    s2 : List A
    s3 : List A
    nemp : s2 ≢ []
    leqs : l ≡ s1 ++ s2 ++ s3
    bnd : length s1 + length s2 ≤ pc r
    pmp : (n : ℕ) → (s1 ++ rep n s2 ++ s3) =~ r

plProof : {A : Set ℓ} (l : List A) (r : RegExp A)
  → l =~ r → pc r ≤ length l → pumpingLemma l r
```

Agda correctly omits any case involving `∅` since no string can ever match it. 
And case matching on `p : pc r ≤ length l` correctly marks the case for `[ a ]ᵣ`
as asbsurd. The pumoing lemma doesn't apply to [ x ]ᵣ because pumping constant is greater
than any string that would match it.

```agda
plProof .(a ∷ []) .([ a ]ᵣ) (M[] a) (s≤s ())
plProof .(xs ++ ys) .(r +ᵣ s) (M+ xs ys r s m1 m2) p with +≤-split (pc r) (pc s) lx ly q
  where
    lx ly : ℕ 
    lx = length xs
    ly = length ys
    q : pc r + pc s ≤ lx + ly
    q = subst (λ z → pc r + pc s ≤ z) (++-length xs ys) p
```
So we break the inequality given into two cases.
Either `xs` and `r` satisfy the pumping lemma, or `ys` and `s` do. 
```agda
plProof .(xs ++ ys) .(r +ᵣ s) (M+ xs ys r s m1 m2) p | inj₁ q with plProof xs r m1 q
... | record { s1 = s1 ; s2 = s2 ; s3 = s3 ; nemp = nemp ; leqs = leqs ; bnd = bnd ; pmp = pmp }
  = record { s1 = s1 ; s2 = s2 ; s3 = s3 ++ ys ; nemp = nemp ;
             leqs = xs ++ ys ≡⟨ cong (_++ ys) leqs ⟩
                    (s1 ++ s2 ++ s3) ++ ys ≡⟨ ++-4-l s1 s2 s3 ys ⟩
                    s1 ++ s2 ++ s3 ++ ys ∎ ;
             bnd = ≤-tran bnd (≤-+-r (pc r) (pc s)) ;
             pmp = f }
               where
                 f : (n : ℕ) → (s1 ++ rep n s2 ++ s3 ++ ys) =~ (r +ᵣ s)
                 f n = subst (_=~ (r +ᵣ s)) (++-4-l s1 (rep n s2) s3 ys) (M+ (s1 ++ rep n s2 ++ s3) ys r s (pmp n) m2)
plProof .(xs ++ ys) .(r +ᵣ s) (M+ xs ys r s m1 m2) p | inj₂ (qx , qy) with plProof ys s m2 qy
... | record { s1 = s1 ; s2 = s2 ; s3 = s3 ; nemp = nemp ; leqs = leqs ; bnd = bnd ; pmp = pmp }
  = record { s1 = xs ++ s1 ; s2 = s2 ; s3 = s3 ; nemp = nemp ;
             leqs = xs ++ ys ≡⟨ cong (xs ++_) leqs ⟩
                    xs ++ s1 ++ s2 ++ s3 ≡⟨ sym (++-assoc xs s1 (s2 ++ s3)) ⟩ 
                    (xs ++ s1) ++ s2 ++ s3 ∎ ;
             bnd = subst (_≤ pc r + pc s) η (+≤-mon (length xs) (length s1 + length s2) (pc r) (pc s) qx bnd) ;
             pmp = f }
               where
                 η : length xs + (length s1 + length s2) ≡ length (xs ++ s1) + length s2
                 η = length xs + (length s1 + length s2) ≡⟨ +-assoc (length xs) (length s1) (length s2) ⟩
                     length xs + length s1 + length s2  ≡⟨ cong (_+ length s2) (sym (++-length xs s1)) ⟩
                     length (xs ++ s1) + length s2 ∎
                 f : (n : ℕ) → ((xs ++ s1) ++ rep n s2 ++ s3) =~ (r +ᵣ s)
                 f n = subst (_=~ (r +ᵣ s)) (sym (++-assoc xs s1 (rep n s2 ++ s3))) (M+ xs (s1 ++ rep n s2 ++ s3) r s m1 (pmp n))
plProof l .(r ∪ᵣ s) (M∪ₗ .l r s m) p with ⊔≤ (pc r) (pc s) (length l) p
... | pr , _ with plProof l r m pr
... | record { s1 = s1 ; s2 = s2 ; s3 = s3 ; nemp = nemp ; leqs = leqs ; bnd = bnd ; pmp = pmp }
  = record { s1 = s1 ; s2 = s2 ; s3 = s3 ; nemp = nemp ; leqs = leqs ;
             bnd = ≤-tran bnd (⊔-≤-r (pc r) (pc s)) ;
             pmp = λ n → M∪ₗ (s1 ++ rep n s2 ++ s3) r s (pmp n) }
plProof l .(r ∪ᵣ s) (M∪ᵣ .l r s m) p with ⊔≤ (pc r) (pc s) (length l) p
... | _ , ps with plProof l s m ps
... | record { s1 = s1 ; s2 = s2 ; s3 = s3 ; nemp = nemp ; leqs = leqs ; bnd = bnd ; pmp = pmp } 
  = record { s1 = s1 ; s2 = s2 ; s3 = s3 ; nemp = nemp ; leqs = leqs ;
             bnd = ≤-tran bnd (⊔-≤-l (pc s) (pc r)) ;
             pmp = λ n → M∪ᵣ (s1 ++ rep n s2 ++ s3) r s (pmp n) }
```
The `M*Z` case is absurd because the string length is bounded by below by the pumping constant,
which is always positive, but in the `M*Z` case the string length is zero. 
```agda
plProof .[] .(r *) (M*Z r) p with ≤-z-is-z p
... | q = ⊥! (pc-pos r q)
```
```agda
plProof .([] ++ ys) .(r *) (M*S [] ys r m1 m2) p = plProof ys (r *) m2 p
plProof .((x ∷ xs) ++ ys) .(r *) (M*S (x ∷ xs) ys r m1 m2) p with ≤-di (pc r) (length (x ∷ xs))
plProof .((x ∷ xs) ++ ys) .(r *) (M*S (x ∷ xs) ys r m1 m2) p | inj₁ p' with plProof (x ∷ xs) r m1 p'
... | record { s1 = s1 ; s2 = s2 ; s3 = s3 ; nemp = nemp ; leqs = leqs ; bnd = bnd ; pmp = pmp }
  = record { s1 = s1 ; s2 = s2 ; s3 = s3 ++ ys ; nemp = nemp ;
             leqs = x ∷ xs ++ ys ≡⟨ cong (_++ ys) leqs ⟩
                    (s1 ++ s2 ++ s3) ++ ys ≡⟨ ++-4-l s1 s2 s3 ys ⟩
                    s1 ++ s2 ++ s3 ++ ys ∎ ;
             bnd = bnd ;
             pmp = f}
               where
                 f : (n : ℕ) → (s1 ++ rep n s2 ++ s3 ++ ys) =~ (r *)
                 f n = subst (_=~ (r *)) (++-4-l s1 (rep n s2) s3 ys) (M*S (s1 ++ rep n s2 ++ s3) ys r (pmp n) m2)
```
And in this final subcase of `M*Z` we construct the proof directly.
```agda
plProof .((x ∷ xs) ++ ys) .(r *) (M*S (x ∷ xs) ys r m1 m2) p | inj₂ p'
  = record { s1 = [] ; s2 = x ∷ xs ; s3 = ys ; nemp = u ; leqs = refl ;
             bnd = ≤-tran (s≤s (≤-step (length xs))) p' ;
             pmp = f }
               where
                 u : (x ∷ xs) ≢ []
                 u p = snotz (cong length p)
                 f : (n : ℕ) → (rep n (x ∷ xs) ++ ys) =~ (r *)
                 f zero = m2
                 f (suc n) = subst (_=~ (r *)) (sym (++-assoc (x ∷ xs) (rep n (x ∷ xs)) ys)) (M*S (x ∷ xs) (rep n (x ∷ xs) ++ ys) r m1 (f n))
