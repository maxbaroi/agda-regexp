# Verified Regular Expressions in Agda (revisited) - Part 2

We introduced a lot last time so I figured I best break up my post in two parts.
The first part deals with the defining an inductive datatype to capture the
notion of a string matching a regular expression and then proving the pumping lemma.
In this part we create a predicate to determine if a string matches a regular expression
based on the [Brzozowski derivative](https://en.wikipedia.org/wiki/Brzozowski_derivative)
of a regular expression.
And finally, we prove these two rapproaches coincide. 
```agda
{-# OPTIONS --cubical #-}

open import RegularExpressions




