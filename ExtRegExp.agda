{-# OPTIONS --cubical #-}
module ExtRegExp where


open import Cubical.Foundations.Prelude
  using (_≡_
        ; refl
        ; cong
        ; sym
        ; _∙_
        ; subst
        ; step-≡
        ; _∎
        ; funExt
        ; Level
        ; ℓ-max)

open import Cubical.Relation.Nullary
  using (Dec ; no ; yes ;  ¬_ )

open import Cubical.Data.Empty
  renaming (rec to ⊥!)

variable
  ℓ : Level

open import Data.List as List
  using (List ; _∷_ ; [] ; _++_ ; length)

open import Data.Nat
  using (ℕ ; suc ; zero) -- ;  _+_ ; _≤_ ; z≤n ; s≤s ; _⊔_ ; _<_ )
open import Cubical.Data.Nat
  using (snotz)

open import Data.Product
  using (_×_ ; ∃ ; Σ-syntax ; _,_)

open import Data.Sum
  using (_⊎_ ; inj₁ ; inj₂)

open import Data.Bool
  using (Bool ; true ; false; _∧_ ; _∨_ ; not)

{- open import Function.Base
  using (_∘_) -}

data RegExp (A : Set ℓ) : Set ℓ where
  EmptySet : RegExp A
  EmptyStr : RegExp A
  Char : A → RegExp A
  App : RegExp A → RegExp A → RegExp A
  Union : RegExp A → RegExp A → RegExp A
  Intersection : RegExp A → RegExp A → RegExp A
  Not : RegExp A → RegExp A
  Star : RegExp A → RegExp A


DecEq : (A : Set ℓ) → Set ℓ
DecEq A = (x y : A) → Dec (x ≡ y)


matchesEmpty : {A : Set ℓ} -> RegExp A → Bool
matchesEmpty EmptySet = false
matchesEmpty EmptyStr = true
matchesEmpty (Char _) = false
matchesEmpty (App re₁ re₂) = matchesEmpty re₁ ∧ matchesEmpty re₂
matchesEmpty (Union re₁ re₂) = matchesEmpty re₁ ∨ matchesEmpty re₂
matchesEmpty (Intersection re₁ re₂) = matchesEmpty re₁ ∧ matchesEmpty re₂
matchesEmpty (Not re) = not (matchesEmpty re)
matchesEmpty (Star re) = true

data NullSet (A : Set ℓ) : RegExp A → Set ℓ
data NullStr (A : Set ℓ) : RegExp A → Set ℓ

data NullSet A where
  NSet : NullSet A EmptySet
  NChar : (a : A) → NullSet A (Char a)
  NApp1 : ( r s : RegExp A) → NullSet A r → NullSet A (App r s)
  NApp2 : ( r s : RegExp A) → NullSet A s → NullSet A (App r s)
  NUnion : (r s : RegExp A) → NullSet A r → NullSet A s → NullSet A (Union r s)
  NInter1 : ( r s : RegExp A) → NullSet A r → NullSet A (Intersection r s)
  NInter2 : ( r s : RegExp A) → NullSet A s → NullSet A (Intersection r s)
  NNotSet : (r : RegExp A) → NullStr A r → NullSet A (Not r)

data NullStr A where
  NStr : NullStr A EmptyStr
  NApp : (r s : RegExp A) → NullStr A r → NullStr A s → NullStr A (App r s)
  NUnionL : (r s : RegExp A) → NullStr A r → NullStr A (Union r s)
  NUnionR : (r s : RegExp A) → NullStr A s → NullStr A (Union r s)
  NInter : (r s : RegExp A) → NullStr A r → NullStr A s → NullStr A (Intersection r s)
  NNotStr : (r : RegExp A) → NullSet A r → NullStr A (Not r)
  NStar : (r : RegExp A) → NullStr A (Star r)

decNull : (A : Set ℓ) → (r : RegExp A) → NullSet A r ⊎ NullStr A r
decNull A EmptySet = inj₁ NSet
decNull A EmptyStr = inj₂ NStr
decNull A (Char a) = inj₁ (NChar a)
decNull A (App r s) with decNull A r
decNull A (App r s) | inj₁ p = inj₁ (NApp1 r s p)
decNull A (App r s) | inj₂ p with decNull A s
decNull A (App r s) | inj₂ p | inj₁ q = inj₁ (NApp2 r s q)
decNull A (App r s) | inj₂ p | inj₂ q = inj₂ (NApp r s p q)
decNull A (Union r s) with decNull A r
decNull A (Union r s) | inj₂ p = inj₂ (NUnionL r s p)
decNull A (Union r s) | inj₁ p with decNull A s
decNull A (Union r s) | inj₁ p | inj₂ q = inj₂ (NUnionR r s q)
decNull A (Union r s) | inj₁ p | inj₁ q = inj₁ (NUnion r s p q)
decNull A (Intersection r s) with decNull A r
decNull A (Intersection r s) | inj₁ p = inj₁ (NInter1 r s p)
decNull A (Intersection r s) | inj₂ p with decNull A s
decNull A (Intersection r s) | inj₂ p | inj₁ q = inj₁ (NInter2 r s q)
decNull A (Intersection r s) | inj₂ p | inj₂ q = inj₂ (NInter r s p q)
decNull A (Not r) with decNull A r
decNull A (Not r) | inj₁ p = inj₂ (NNotStr r p)
decNull A (Not r) | inj₂ p = inj₁ (NNotSet r p)
decNull A (Star r) = inj₂ (NStar r)


regDeriv : {A : Set ℓ} → (eq : DecEq A) → A → RegExp A → RegExp A
regDeriv _ _ EmptySet = EmptySet
regDeriv _ _ EmptyStr = EmptySet
regDeriv eq a (Char b) with eq a b
regDeriv eq a (Char b) | yes p = EmptyStr
regDeriv eq a (Char b) | no ¬p = EmptySet
{- Double check derivative definition to replace App EmptyStr _ with App (aux re₁) -}
{- regDeriv eq a (App re₁ re₂) = Union (App (regDeriv eq a re₁) re₂) (App EmptyStr (regDeriv eq a re₂)) -}
{-regDeriv eq a (App re₁ re₂) = Union (App (regDeriv eq a re₁) re₂) (regDeriv eq a re₂) -}
{- regDeriv eq a (App re₁ re₂) = Union (App (regDeriv eq a re₁) re₂) (App (aux re₁) (regDeriv eq a re₂)) -}
regDeriv eq a (App r s) with decNull _ r
regDeriv eq a (App r s) | inj₁ p = Union (App (regDeriv eq a r) s) (App EmptySet (regDeriv eq a s))
regDeriv eq a (App r s) | inj₂ q = Union (App (regDeriv eq a r) s) (App EmptyStr (regDeriv eq a s))
regDeriv eq a (Union r s) = Union (regDeriv eq a r) (regDeriv eq a s)
regDeriv eq a (Intersection r s) = Intersection (regDeriv eq a r) (regDeriv eq a s)
regDeriv eq a (Not r) = Not (regDeriv eq a r)
regDeriv eq a (Star r) = App (regDeriv eq a r) (Star r)

recognizer : {A : Set ℓ} → (eq : DecEq A) → List A → RegExp A → Bool
recognizer eq [] r with decNull _ r
recognizer eq [] r | inj₁ p = false
recognizer eq [] r | inj₂ q = true
recognizer eq (a ∷ l) r = recognizer eq l (regDeriv eq a r)


{-
matchesAux : {A : Set ℓ} → RegExp A → RegExp A
matchesAux re = {!!}

{- Brzozwski derivative -}
bDeriv : {A : Set ℓ} → (eq : DecEq A) → A → RegExp A → RegExp A
bDeriv _ _ EmptySet = EmptySet
bDeriv _ _ EmptyStr = EmptySet
bDeriv eq a (Char b) with eq a b
bDeriv eq a (Char b) | yes p = EmptyStr
bDeriv eq a (Char b) | no ¬p = EmptySet
bDeriv eq a (App re₁ re₂) = Union (App (bDeriv eq a re₁) re₂) (App EmptyStr (bDeriv eq a re₂))
bDeriv eq a (Union re₁ re₂) = Union (bDeriv eq a re₁) (bDeriv eq a re₂)
bDeriv eq a (Intersection re₁ re₂) = Intersection (bDeriv eq a re₁) (bDeriv eq a re₂)
bDeriv eq a (Not re) = Not (bDeriv eq a re)
bDeriv eq a (Star re) = App (bDeriv eq a re) (Star re)

recognizer : {A : Set ℓ} → (eq : DecEq A) → List A → RegExp A → Bool
recognizer eq [] re = matchesEmpty re
recognizer eq (a ∷ l) re = recognizer eq l (bDeriv eq a re)
-}


{-
data ExpMatch {A : Set ℓ} : List A → RegExp A → Set ℓ where
  MEmptyStr : ExpMatch [] EmptyStr
  MChar : (x : A) → ExpMatch (x ∷ []) (Char x)
  MApp : (l₁ l₂ : List A) (re₁ re₂ : RegExp A)
       → ExpMatch l₁ re₁ → ExpMatch l₂ re₂ 
       → ExpMatch (l₁ ++ l₂) (App re₁ re₂)
  MUnionL : (l : List A) (re₁ re₂ : RegExp A)
    → ExpMatch l re₁ → ExpMatch l (Union re₁ re₂)
  MUnionR : (l : List A) (re₁ re₂ : RegExp A)
    → ExpMatch l re₂ → ExpMatch l (Union re₁ re₂)
  MStarZ : (re : RegExp A) → ExpMatch [] (Star re)
  MStarS : (l₁ l₂ : List A) (re : RegExp A)
    → ExpMatch l₁ re → ExpMatch l₂ (Star re)
    → ExpMatch (l₁ ++ l₂) (Star re)
--  MNot : (l : List A) (re : RegExp A) → ¬ (ExpMatch l re)
--    → ExpMatch l (Not re)
-}

{-
data _=~_ {A : Set ℓ} {d : DecEq A} : List A → RegExp A → Set ℓ where
  MNot : (l : List A) (re : RegExp A) → ¬ (l =~ re) → l =~ Not re
-}
